export default {
  state: {
    posts: [],
    edit: false,
  },
  mutations: {
    updatePosts(state, posts) {
      state.posts = posts
    },
    createPost(state,newPost) {
      state.posts.unshift(newPost)
    },
    deletePost(state, id) {
      return state.posts = state.posts.filter(post => post.id !== id)
    },
  },
  actions: {
    async fetchPosts({ commit }) {
      const res = await fetch('https://jsonplaceholder.typicode.com/todos?_limit=1')
      const posts = await res.json()
      commit('updatePosts', posts)
    },
    async deletePost({ commit }, id) {
      await fetch(`https://jsonplaceholder.typicode.com/todos?_limit=${id}`)
      commit('deletePost', id)
    },
  },
  getters: {
    allPosts(state) {
      return state.posts
    },
    validPosts(state) {
      return state.posts.filter(p => {return p.title})
    },
    postCount(state, getters) {
      return getters.validPosts.length
    }
  }
}
